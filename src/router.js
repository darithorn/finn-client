var finn = finn || {};

// A route will consist of:
// - a path
// - a template
// - a controller

// new Router ({ "/": { "home", function () {} } });
// route ("/", "home", function () {});
finn.Router = function (routes) {
	this.routes = routes || {};
	this.current = null;

	var that = this;
	window.addEventListener ("hashchange", function () {
		that.direct ();
	});
	window.addEventListener ("load", function () {
		that.direct ();
	});
};

finn.Router.prototype.direct = function () {
	var url = location.hash.slice (1) || "/";
	var route = null;
	var result = null;
	var exp = "";
	for (exp in this.routes) {
		var regex = new RegExp ("^" + exp + "$");
		result = regex.exec (url);
		if (result !== null) {
			route = this.routes[exp];
			break;
		}
	}
	if (route) {
		if (this.current && this.current.controller) {
			this.current.controller._unload ();
		}
		this.current = route;
		$ (document).ready (function () {
			route.view._render ();
			route.view._initialize ();
			if (route.controller) {
				route.controller._initialize ();
				route.controller._attachEvents ();
			}
		});
	} else {
		$.get ("templates/notfound.mst", function (template) {
			$ (document).ready (function () {
				$ ("#view").html (Mustache.render (template, {}));
			});
		});
	}
};

finn.Router.prototype.route = function (route, view, controller) {
	this.routes[route] = { view: view, controller: controller };
};
