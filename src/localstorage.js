var finn = finn || {};

// Default storage will be local
finn.LocalStorage = function (model) {
	if (typeof (Storage) === "undefined") {
		throw new Error ("Local storage isn't supported!");
	}
	this.model = model;
};

finn.LocalStorage.prototype.sync = function () {
	if (typeof (Storage) === "undefined") {
		throw new Error ("Local storage isn't supported!");
	}
	for (var key in this.model.values) {
		window.localStorage.setItem (key, this.model.values[key]);
	}
};

finn.LocalStorage.prototype.destroy = function () {
	if (typeof (Storage) === "undefined") {
		throw new Error ("Local storage isn't supported!");
	}
	for (var key in this.model.values) {
		window.localStorage.removeItem (key);
	}
}

finn.LocalStorage.prototype.retrieve = function () {
	var values = {};
	for (var key in this.model.defaults) {
		var v = window.localStorage.getItem (key);
		if (v !== null) {
			switch (typeof (this.model.defaults[key])) {
				case "number":
					values[key] = Number (v);
					break;
				case "boolean":
					values[key] = Boolean (v);
					break;
				default:
					values[key] = v;
					break;
			}
		}
	}
	return values;
}
