var finn = finn || {};

// A way to sync the model information with a database
// Local storage or sending information to the server

// A method called 'sync' or 'save' that the model implements
// There needs to be a default

// Ability to create your own storage backend
// And easily set each model's storage backend

finn.Model = function (hash) {
	this.defaults = {};
	this.view = null;

	// Example:
	// {
	//    "change": [ "onChange" ],
	//    "change counter": [ "onCounterChange" ],
	// }
	this.listeners = {};
	this.values = {};

	for (var key in hash) {
		this[key] = hash[key];
	}

	if (this.defaults !== {}) {
		// Add all the default values to the model
		for (var key in this.defaults) {
			this.values[key] = this.defaults[key];
		}
	}

};

// Broadcasts values to all the listeners
finn.Model.prototype._broadcast = function () {
	for (var key in this.listeners) {
		for (var f in this.listeners[key]) {
			this [this.listeners[key][f]] ();
		}
	}
}

// "change" -- listen to any value change
// "change [field]" -- only listens to that field
finn.Model.prototype.listen = function (listener, func) {
	var listenerLength = listener.split (" ").length;
	if (listenerLength === 0 || listenerLength > 2) {
		throw new Error (listener + " is not a valid listener identifier!");
	}
	if (this.listeners[event] === undefined) {
		this.listeners[event] = [ func ];
	} else {
		this.listeners[event].push (func);
	}
	// TODO: Move logic from _broadcast here?
};

finn.Model.prototype.get = function (field) {
	return this.values[field];
};

finn.Model.prototype.resetValues = function () {
	for (var key in this.defaults) {
		this.set (key, this.defaults[key]);
	}
};

finn.Model.prototype.set = function (field, value) {
	this.values[field] = value;
	var listener = "change " + field;
	if (this.listeners[listener] !== undefined) {
		for (var f in this.listeners[listener]) {
			this[this.listeners[listener][f]] ();
		}
	}
};

