var finn = finn || {};

finn.RESTStorage = function (hash) {
	for (var key in hash) {
		this[key] = hash[key];
	}
};

finn.RESTStorage.prototype._sendRequest = function (type, url, data, success) {
	var obj = {
		method: type,
		url: url,
		success: success
	};
	if (type !== "GET") {
		obj.data = JSON.stringify (data);
		obj.dataType = "json";
	}
	$.ajax (obj);
};

finn.RESTStorage.prototype.get = function (url, callback) {
	this._sendRequest ("GET", url, null, callback);
};

finn.RESTStorage.prototype.put = function (url, data, callback) {
	this._sendRequest ("PUT", url, data, callback);
};

finn.RESTStorage.prototype.post = function (url, data, callback) {
	this._sendRequest ("POST", url, data, callback);
};

finn.RESTStorage.prototype.delete = function (url, callback) {
	this._sendRequest ("DELETE", url, callback);
};
