var finn = finn || {};

finn.View = function (hash) {
	this.el = "";
	this.template = "";

	for (var key in hash) {
		this[key] = hash[key];
	}

	var that = this;
	if (typeof (this.templateId) !== "string") {
		this.template = {};
		for (var key in this.templateId) {
			var id = this.templateId[key];
			(function (key, id) {
				$.get (id, function (template) {
					that.template[key] = template;
				});
			}) (key, id);
		}
	} else {
		$.get (this.templateId, function (template) {
			that.template = template;
		})
	}
};

finn.View.prototype._initialize = function () {
	if (this.initialize !== undefined) {
		this.initialize ();
	}
};

finn.View.prototype._render = function () {
	if (this.render !== undefined) {
		this.render ();
	}
};
