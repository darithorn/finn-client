var finn = finn || {};

finn.Controller = function (hash) {
	this.models = {};
	this.events = {};

	for (var key in hash) {
		this[key] = hash[key];
	}
};

finn.Controller.prototype._initialize = function () {
	if (this.load !== undefined) {
		this.load ();
	}
	for (var key in this.models) {
		this.models[key]._broadcast ();
	}
}

finn.Controller.prototype._unload = function () {
	if (this.unload !== undefined) {
		this.unload ();
	}
}

finn.Controller.prototype._attachEvents = function () {
	var that = this;
	Object.keys (this.events).forEach (function (el, i, arr) {
		var keys = el.split (" ");
		if (keys.length !== 2) {
			throw new Error ("Event key must follow 'event_id element_id'!");
		}
		var event_id = keys[0];
		var element_id = keys[1];
		$ (keys[1]).on (keys[0], function () {
			that[that.events[el]] ();
		});
	});
};
