module.exports = function (grunt) {
	grunt.initConfig ({
		pkg: grunt.file.readJSON ("package.json"),
		uglify: {
			finn_client: {
				files: {
					"dist/finn.client.min.js": [
						"src/controller.js",
						"src/localstorage.js",
						"src/model.js",
						"src/reststorage.js",
						"src/router.js",
						"src/view.js"
					]
				}
			}
		}
	});

	grunt.loadNpmTasks ("grunt-contrib-uglify");
};
